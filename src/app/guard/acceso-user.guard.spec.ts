import { TestBed } from '@angular/core/testing';

import { AccesoUserGuard } from './acceso-user.guard';

describe('AccesoUserGuard', () => {
  let guard: AccesoUserGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AccesoUserGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
