import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccesoUserGuard implements CanActivate {
  record: number;
  constructor(private router:Router){}


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {

    this.record = +route.paramMap.get('Score');
    console.log(this.record+ ' Es el score');
    if (this.record < 30) {
      this.router.navigate(['']);
      console.log(this.record+ ' El score es menor a 30 acceso denegado');
      return false;
    }
    return true;
  }
}
