import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
@Component({
  selector: 'app-detalle-usuario',
  templateUrl: './detalle-usuario.component.html',
  styleUrls: ['./detalle-usuario.component.scss']
})
export class DetalleUsuarioComponent implements OnInit {

  idU = '';
  imagen = '';
  nombre = '';
  usuarioF:any = [];
  usuarioA:any = [];
  constructor(public route: ActivatedRoute, public router: Router,private http: HttpClient) { 
    this.route.params.subscribe(parametros => {
      this.idU = parametros['idU'];
    });
  }

  ngOnInit(): void {
    this.getUser();
  }
  getUser(){
    this.http.get('https://api.github.com/users/'+this.idU).subscribe(data => {
      this.usuarioA = this.usuarioF.push(data);
      console.log(this.usuarioA);
    });
    console.log("Esto se ejecutará antes que el console log de arriba");
  }
  volver()
  {
    this.router.navigate(['/Listado']);
  }
}
