import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-listado-usuarios',
  templateUrl: './listado-usuarios.component.html',
  styleUrls: ['./listado-usuarios.component.scss']
})
export class ListadoUsuariosComponent implements OnInit {
  chart:any;
  roomForm: FormGroup;
  mensajes_validacion = {
    'nota': [
      { type: 'required', message: 'El campo es requerido' },
      { type: 'minlength', message: '4 caracteres minimos' }
    ],
  }
  usuariosS: any = [];
  usuariosF: any = [];
  usuariosB: any = [];
  DataChart: any = [];
  constructor(private fb: FormBuilder, private http: HttpClient) { 
    this.roomForm = this.fb.group({
      Note: ["", Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ])],
    });
  }

  ngOnInit(): void {
    this.roomForm.get('Note').setValue('');
    this.getUsers();
    this.grafica();
  }
  getUsers(){
    this.http.get('https://api.github.com/search/users?q=YOUR_NAME').subscribe(data => {
      this.usuariosS = data;
      this.usuariosF =this.usuariosS.items;
      console.log(this.usuariosF);
      for (let i = 0; i < 11; i++) {
        this.DataChart.push(this.usuariosF[i].score);
        console.log(this.DataChart);
      }
    });
    console.log("Esto se ejecutará antes que el console log de arriba");
  }
  onSubmit() {
    if(this.roomForm.get('Note').value === 'gcpglobal')
    {
      Swal.fire('El nombre está deshabilitado');
    }
    if(this.roomForm.get('Note').value !== 'gcpglobal')
    {
      console.log(this.usuariosF);
      this.usuariosB = this.usuariosF.filter(element => element.login === this.roomForm.get('Note').value );
      this.usuariosF = this.usuariosB;
      console.log(this.usuariosF);
    }
  }
  grafica(){
    // var canvas = this.el;
    this.chart = new Chart('canvas', {
    type: 'bar',
    data: {
      labels: ["Seguidores 1","Seguidores 2","Seguidores 3","Seguidores 4","Seguidores 5","Seguidores 6","Seguidores 7","Seguidores 8","Seguidores 9","Seguidores 10"],
      datasets: [{
        label: '',
        data: this.DataChart,
        backgroundColor: [
          'rgba(111, 172, 83, 1)',
          'rgba(247, 224, 13, 1)',
          'rgba(222, 145, 47, 1)',
          'rgba(55, 167, 156, 1)',
          'rgba(255, 191, 166, 1)',
          'rgba(102, 168, 222, 1)',
          'rgba(203, 34, 40, 1)'
        ],
        borderColor: [
          'rgba(111, 172, 83, 1)',
          'rgba(247, 224, 13, 1)',
          'rgba(222, 145, 47, 1)',
          'rgba(55, 167, 156, 1)',
          'rgba(255, 191, 166, 1)',
          'rgba(102, 168, 222, 1)',
          'rgba(203, 34, 40, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      title: {
        display: false
      },
      legend: {
        display:false
      },
      scales: {
        yAxes: [{
            ticks: {
             fontSize: 25,
             fontStyle: 'normal',
             beginAtZero:true,
             precision:0
            },
            gridLines: {
              zeroLineWidth:5,
              zeroLineColor:'rgba(166, 197, 212, 1)'
          }
        }],
        xAxes: [{
         ticks: {
          fontSize: 20,
          fontStyle: 'normal',
          beginAtZero:true
         },
         gridLines: {
          zeroLineWidth:5,
          zeroLineColor:'rgba(166, 197, 212, 1)'
      }
     }]
    },
    }
    })
    }
}
