import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListadoUsuariosComponent} from './listado-usuarios/listado-usuarios.component';
import { DetalleUsuarioComponent } from './detalle-usuario/detalle-usuario.component';
import { AccesoUserGuard } from './guard/acceso-user.guard';
const routes: Routes = [
  {
    path: '',
    component: ListadoUsuariosComponent,
    },
    {
    path: 'Listado',
    component: ListadoUsuariosComponent,
    },
    {
      path: 'DetalleUsuario/:idU/:Score',
      component: DetalleUsuarioComponent,
      canActivate: [AccesoUserGuard]
    }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
